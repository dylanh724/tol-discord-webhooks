# tol-discord-webhooks

Webhook examples for rich embeds in Discord

# April Fools
![](https://i.imgur.com/GBksZ1b.png)

```json
{
  "username" : "The Fool",
  "content" : "\"Ehehehh-- @everyone will see the *true* fools revealed today... HAHAHHAH...!\"",
  "embeds": 
  [{
    "title": "The Fool is up to something...",
    "color": 65463,
    "footer": {
      "text": "Already logged in? You may want to restart~"
    },
    "image": {
      "url": "https://i.imgur.com/ywD59hA.png"
    },
    "author": {
      "name": "<Click Here to Launch Throne>",
      "url": "https://toli.es/launch",
      "icon_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Steam_icon_logo.svg/512px-Steam_icon_logo.svg.png"
    }
  }]
}
```